const bcrypt = require('bcrypt')
const SALT_ROUNDS = 10

const jwt = require('jsonwebtoken')
const dotenv = require('dotenv')
dotenv.config()
const TOKEN_SECRET = process.env.TOKEN_SECRET

const mysql = require('mysql')

const connection = mysql.createConnection({
    host : 'localhost',
    user : 'runner_admin',
    password : 'runner_admin',
    database : 'RuningSystem'
})

connection.connect();

const express =require('express')
const jsonwebtoken = require('jsonwebtoken')
const app = express()
const port = 4000

/* ========= Middlewar ===============*/
function autheticateToken(req , res , next){
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]
    if (token == null ) return res.sendStatus(401)
    jwt.verify(token,TOKEN_SECRET, (err,user) => {
        if(err) { return res.sendStatus(403) }
        else {
            req.user = user
            next()
        }
    })
}
/*===========================================*/
/* ========= API FOR Reister Runner ========= */

app.get("/List_runner", (req,res) =>{
    let query = "SELECT * from Runner";
    connection.query(query,(err,rows) => {
        if (err){
            res.json({
                    "status" : "400",
                    "message" : "Error querying from running db"
            })
        }else{
            res.json(rows);
        }
    });
})


app.post("/register_runner" , (req,res) => {
    let runner_name = req.query.runner_name
    let runner_surname = req.query.runner_surname
    let runner_username = req.query.runner_username
    let runner_password = req.query.runner_password
    bcrypt.hash(runner_password,SALT_ROUNDS,(err,hash) => {
        let query = `INSERT INTO Runner
                    (RunnerName,RunnerSurname,Username,Password,IsAdmin)  
                    VALUES ('${runner_name}','${runner_surname}',
                            '${runner_username}','${hash}',false )`
        console.log(query)
        connection.query(query,(err,rows) => {
        if (err){
            res.json({
                    "status" : "400",
                    "message" : "Error querying from running db"
                })
        }else{
            res.json({
                    "status" : "200",
                    "message" : "Add new user succesful"
            });
        }
    })
    })

})

app.post("/update_runner", (req,res) =>{
    let runner_id = req.query.runner_id
    let runner_name = req.query.runner_name
    let runner_surname = req.query.runner_surname

    let query = `UPDATE Runner SET
                    RunnerName='${runner_name}',
                    RunnerSurname='${runner_surname}'
                    WHERE RunnerID=${runner_id}`

    console.log(query)                
                    
    connection.query(query,(err,rows) => {
        console.log(err)
        if (err){
            res.json({
                    "status" : "400",
                    "message" : "Error updating unrecord"
                })
        }else{
            res.json({
                    "status" : "200",
                    "message" : "Updating event succesful"
            });
        }
    })

})

app.post("/delete_runner", (req,res) =>{
    let runner_id = req.query.runner_id

    let query = `DELETE FROM Runner WHERE RunnerID=${runner_id}`

    console.log(query)                
                    
    connection.query(query,(err,rows) => {
        console.log(err)
        if (err){
            res.json({
                    "status" : "400",
                    "message" : "Error updating unrecord"
                })
        }else{
            res.json({
                    "status" : "200",
                    "message" : "Updating event succesful"
            });
        }
    })

})


/* LOGIN */
app.post("/login" , (req,res) => {
    let username = req.query.username
    let user_password = req.query.user_password
    let query = `SELECT * FROM Runner WHERE Username ='${username}'`

    connection.query( query,(err,row) => {
        if(err){
            console.log(err)
            res.json({
                "status" : "400",
                "message" : "Error querying from running db"
            })
        }else {
            let db_password = row[0].Password
            bcrypt.compare(user_password, db_password,  (err, result) =>{
                if(result) { 
                    let payload = {
                        "username" : row[0].Username,
                        "user_id" : row[0].RunnerID,
                        "IsAdmin" : row[0].isAdmin
                    }
                    console.log(payload)
                    let token = jwt.sign(payload, TOKEN_SECRET , {expiresIn : '1d'})
                    res.send(token)
                    
                }else {res.send("Invalid username/password")}
            })
        
        }
    })
})



/*  ========= CRUD Operation for RinningEvent ========= */
app.get("/List_event", (req,res) =>{
    let query = "SELECT * from RuningEvent";
    connection.query(query,(err,rows) => {
        if (err){
            res.json({
                    "status" : "400",
                    "message" : "Error querying from running db"
            })
        }else{
            res.json(rows);
        }
    });
})

app.post("/add_event", (req,res) =>{
    let event_name = req.query.event_name
    let event_location = req.query.event_location

    let query = `INSERT INTO    RuningEvent (EventName,EventLocation) 
                                VALUES ('${event_name}','${event_location}')`
    
    connection.query(query,(err,rows) => {
        if (err){
            res.json({
                    "status" : "400",
                    "message" : "Error querying from running db"
                })
        }else{
            res.json({
                    "status" : "200",
                    "message" : "Add event succesful"
            });
        }
    })

})

app.post("/update_event", (req,res) =>{
    let event_id = req.query.event_id
    let event_name = req.query.event_name
    let event_location = req.query.event_location

    let query = `UPDATE RuningEvent SET
                    EventName='${event_name}',
                    EventLocation='${event_location}'
                    WHERE EventID=${event_id}`

    console.log(query)                
                    
    connection.query(query,(err,rows) => {
        console.log(err)
        if (err){
            res.json({
                    "status" : "400",
                    "message" : "Error updating unrecord"
                })
        }else{
            res.json({
                    "status" : "200",
                    "message" : "Updating event succesful"
            });
        }
    })

})

app.post("/delete_event", (req,res) =>{
    let event_id = req.query.event_id

    let query = `DELETE FROM RuningEvent WHERE EventID=${event_id}`

    console.log(query)                
                    
    connection.query(query,(err,rows) => {
        console.log(err)
        if (err){
            res.json({
                    "status" : "400",
                    "message" : "Error updating unrecord"
                })
        }else{
            res.json({
                    "status" : "200",
                    "message" : "Updating event succesful"
            });
        }
    })

})


/*
Query 1 : List All Registrations
SELECT
        Runner.RunnerID,Runner.RunnerName,Runner.RunnerSurname,RuningEvent.EventName,
        Registration.Distance,Registration.RegistrationTime 
FROM 
        Runner,Registration,RuningEvent 
WHERE
        (Registration.RunnerID = Runner.RunnerID)AND
        (Registration.EventID = RuningEvent.EventID);
*/
app.get("/list_reg" , (req,res) => {

    let query = `

    SELECT
        Runner.RunnerID,Runner.RunnerName,Runner.RunnerSurname,RuningEvent.EventName,
        Registration.Distance,Registration.RegistrationTime 
    FROM 
        Runner,Registration,RuningEvent 
    WHERE
        (Registration.RunnerID = Runner.RunnerID)AND
        (Registration.EventID = RuningEvent.EventID);
        
    
        `
    
    connection.query(query,(err,rows) => {
        if (err){
            res.json({
                    "status" : "400",
                    "message" : "Error querying from running db"
                })
        }else{
            res.json(rows)
        }
    })

})
/*   
Query 2 : List ALL Registrations by Event ID
SELECT 
        Runner.RunnerID,Runner.RunnerName,Runner.RunnerSurname,RuningEvent.EventName,
        Registration.Distance,Registration.RegistrationTime 
FROM 
        Runner,Registration,RuningEvent 
WHERE 
        (Registration.RunnerID = Runner.RunnerID)AND
        (Registration.EventID = RuningEvent.EventID)AND
        (RuningEvent.EventID = 1);*/

app.get("/list_reg_event" , (req,res) => {
    let event_id = req.query.event_id

    let query = `

    SELECT 
        Runner.RunnerID,Runner.RunnerName,Runner.RunnerSurname,
        Registration.Distance,Registration.RegistrationTime 
    FROM 
        Runner,Registration,RuningEvent 
    WHERE 
        (Registration.RunnerID = Runner.RunnerID)AND
        (Registration.EventID = RuningEvent.EventID)AND
        (RuningEvent.EventID = ${event_id});
        
    
        `
    
    connection.query(query,(err,rows) => {
        if (err){
            res.json({
                    "status" : "400",
                    "message" : "Error querying from running db"
                })
        }else{
            res.json(rows)
        }
    })

})

/* Query 3 : List All Registrations By Runner ID
SELECT 
        Runner.RunnerID,RuningEvent.EventName,Registration.Distance,Registration.RegistrationTime 
FROM    
        Runner,RuningEvent,Registration 
WHERE   
        (Runner.RunnerID = Registration.RunnerID) AND 
        (Registration.EventID = RuningEvent.EventID) AND 
        (Runner.RunnerID = 1);*/

app.get("/list_reg_runner" ,autheticateToken, (req,res) => {

    let runner_id = req.user.user_id
    if(!req.user.IsAdmin) res.send("You not Admin")
    let query = `

    SELECT 
        Runner.RunnerID,RuningEvent.EventName,Registration.Distance,Registration.RegistrationTime 
    FROM    
        Runner,RuningEvent,Registration 
    WHERE   
        (Runner.RunnerID = Registration.RunnerID) AND 
        (Registration.EventID = RuningEvent.EventID) AND 
        (Runner.RunnerID = ${runner_id})   
        `   
    connection.query(query,(err,rows) => {
        if (err){
            res.json({
                    "status" : "400",
                    "message" : "Error querying from running db"
                })
        }else{
            res.json(rows)
        }
    })

})



/* ========= API FOR Reister event ========= */
app.post("/register_event" , autheticateToken,  (req,res) => {
    let user_profile = req.user
    console.log(user_profile)

    
    let runner_id = req.user.user_id
    let event_id = req.query.event_id
    let distance = req.query.distance
    let query = `INSERT INTO Registration
                    (RunnerID,EventID,Distance,RegistrationTime) 
                    VALUES (${runner_id},
                            ${event_id},
                            ${distance},
                            NOW() )`
    
    console.log(query)

    connection.query(query,(err,rows) => {
        if (err){
            res.json({
                    "status" : "400",
                    "message" : "Error querying from running db"
                })
        }else{
            res.json({
                    "status" : "200",
                    "message" : "succesful"
            });
        }
    })

})




app.listen(port,() =>{
    console.log(`Now starting Runing System Backend ${port} `)
})




/*query = "SELECT * from Runner"
connection.query(query,(err,rows) => {
    if (err){
        console.log(err);
    }else{
        console.log(rows);
    }
});*/
