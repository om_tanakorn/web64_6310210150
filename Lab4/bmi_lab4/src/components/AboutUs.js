
function AboutUs(props) {

    return (
    <div>
        <h2> จัดทำโดย :{props.name}</h2>
        <h3> ติดต่อได้ที่ : {props.contact} </h3>
        <h3> บ้านอยู่ :{props.home}</h3>

    </div>
    );
}

export default AboutUs;