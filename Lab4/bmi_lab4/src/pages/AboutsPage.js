import AboutUs from "../components/AboutUs";
function AboutUsPage(){
    
    return(
        <div>
            <div allgn ="center">
                <h2> คะนะผู้จัดทำ เว็บนี้</h2>
                <AboutUs    name='ธนกร AKA.TheLee'
                            contact='discord'
                            home = 'ซานฟาน'/>
                <hr />

                <AboutUs    name='พรี่คาซึยะ'
                            contact='discord'
                            home = 'ลอสซานโตส'/>
            </div>
        </div>
    );
}

export default AboutUsPage;