import BMIResult from "../components/BMIResult";
import {useState} from "react";

function BMICalPage(){

    const [name, setname] =useState(" ");
    const [bmiResult, setbmiResult] =useState( 0 );
    const [translateResult, settranslateResult] =useState(" ");

    const [height,setheight] = useState("");
    const [weight,setweight] = useState("");

    function calculateBMI(){
        let h = parseFloat(height);
        let w = parseFloat(weight);
        let bmi = w / (h*h);

        setbmiResult(bmi);
        if(bmi >25){
            settranslateResult("อ้วนน");
        }else {
            settranslateResult("หล่อเท่");
        }
    }

    return(
        <div align ="left">
            <div align ="center">
                ยินดีต้อนรับเข้าสู้เว็บคำนวน BMI
                <hr/>

                คุณชื่อ : <input type='text'
                                value={name}
                                onChange={ (e)=>{setname(e.target.value);} }/> <br/>
                สูง(ซม.) : <input type='text'
                                value={height}
                                onChange={ (e)=>{setheight(e.target.value);} } /> <br/>
                น้ำหนัก   : <input type='text'
                                value={weight}
                                onChange={ (e)=>{setweight(e.target.value);} } /> <br/>

                <button onClick={ ()=>{ calculateBMI() } }>คำนวน</button>
                { bmiResult !=0 &&
                    <div>
                        <hr/>
                        ผลการคำนวน

                        <BMIResult
                            name ={name}
                            bmi = {bmiResult}
                            result = {translateResult}/>

                    </div>
                }
            </div>
        </div>
        
    )
    
}

export default BMICalPage;