import logo from './logo.svg';
import './App.css';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import { Box , AppBar, Typography , Toolbar} from '@mui/material';
import {useEffect , useState } from "react" ;
import axios from 'axios'
import ReactWeather, { useOpenWeather } from 'react-open-weather';
import YouTube from 'react-youtube';

function App() {

  //OpenW API KEY : dffd2f2af3a2eff031271c561571a8db

  const [ temperature , settemperature ] = useState();

  
  const weatherAPIBaseUrl = "https://api.openweathermap.org/data/2.5/weather?";
  const city = "Songkhla";
  const apiKey = "dffd2f2af3a2eff031271c561571a8db"


  const { data, isLoading, errorMessage } = useOpenWeather({
    key: 'dffd2f2af3a2eff031271c561571a8db',
    lat: '18.8543',
    lon: '100.4960',
    lang: 'en',
    unit: 'metric', // values are (metric, standard, imperial)
  });

  useEffect( () => {
    settemperature("---");

    axios.get(weatherAPIBaseUrl+"q="+city+"&appid="+apiKey).then( (response)=>{
      let data = response.data
      console.log(data.main.temp);
      let temp = data.main.temp - 273 ;
      settemperature(temp.toFixed(2))
    })
  }
  , [] );
  

  return (
    <Box sx={ { flexGrow : 1, width : "100%" } }>
      <AppBar position='static'>
        <Toolbar>
          <Typography variant = "h6">
            Weather App
          </Typography>

        </Toolbar>
      </AppBar>

      <Box sx = {{ justifyContent : 'center' , marginTop : "10px" , display : "flex"}}>
        <Typography variant = "h4">
          อากาศอาดใหญ่วันนี้
        </Typography>

      </Box>

      <Box sx = {{ justifyContent : 'center' , marginTop : "20px" , display : "flex"}}> 
      <Card sx={{ minWidth: 275 }}>
          <CardContent>
            <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
              อากาศหาดใหญ่วันนี้
            </Typography>

            <Typography>
              {temperature}
            </Typography>

            <Typography variant="body2">
              องศาเซลเซียส
            </Typography>

          </CardContent>
        </Card>
        <ReactWeather
      isLoading={isLoading}
      errorMessage={errorMessage}
      data={data}
      lang="en"
      locationLabel="Sonklar"
      unitsLabels={{ temperature: 'C', windSpeed: 'Km/h' }}
      showForecast
    />
        
        </Box>

        

<YouTube videoId="nja_0BaQcNg" />

    </Box>
  );
}

export default App;
