import BMIResult from "../components/BMIResult";
import {useState} from "react";
import Button from '@mui/material/Button';
import { Grid,Box, Typography,Paper, Container} from "@mui/material";

function BMICalPage(){

    const [name, setname] =useState(" ");
    const [bmiResult, setbmiResult] =useState( 0 );
    const [translateResult, settranslateResult] =useState(" ");

    const [height,setheight] = useState("");
    const [weight,setweight] = useState("");

    function calculateBMI(){
        let h = parseFloat(height);
        let w = parseFloat(weight);
        let bmi = w / (h*h);

        setbmiResult(bmi);
        if(bmi >25){
            settranslateResult("อ้วนน");
        }else {
            settranslateResult("หล่อเท่");
        }
    }

    return(
    <Container maxWidth="lg">
      <Grid container spacing={2} sx={ {marginTop : "10px"} }>
        <Grid item xs={12}>
            <Typography >
                ยินดีต้อนรับเข้าสู้เว็บคำนวน BMI
            </Typography>
        </Grid>

        <Grid item xs={8}>
          <Box sx={{textAlign : "left"}}>
            คุณชื่อ : <input type='text'
                            value={name}
                            onChange={ (e)=>{setname(e.target.value);} }/> <br/><br/>
            สูง(ซม.) : <input type='text'
                            value={height}
                            onChange={ (e)=>{setheight(e.target.value);} } /> <br/><br/>
            น้ำหนัก   : <input type='text'
                            value={weight}
                            onChange={ (e)=>{setweight(e.target.value);} } /> <br/><br/>

            <Button variant="contained" onClick={ ()=>{ calculateBMI() } } >
                คำนวน
            </Button>
          </Box>
        </Grid>

        <Grid item xs={4}>
            { bmiResult !=0 &&
            <div>
                ผลการคำนวน

                <BMIResult
                    name ={name}
                    bmi = {bmiResult}
                    result = {translateResult}/>

            </div>
            }
          
        </Grid>





      </Grid>
      </Container>
  );
}

export default BMICalPage;

/*<div align ="left">
<div align ="center">
    ยินดีต้อนรับเข้าสู้เว็บคำนวน BMI
    <hr/>

    คุณชื่อ : <input type='text'
                    value={name}
                    onChange={ (e)=>{setname(e.target.value);} }/> <br/>
    สูง(ซม.) : <input type='text'
                    value={height}
                    onChange={ (e)=>{setheight(e.target.value);} } /> <br/>
    น้ำหนัก   : <input type='text'
                    value={weight}
                    onChange={ (e)=>{setweight(e.target.value);} } /> <br/>

    <Button variant="contained" onClick={ ()=>{ calculateBMI() } } >
        คำนวน
    </Button>

    { bmiResult !=0 &&
        <div>
            <hr/>
            ผลการคำนวน

            <BMIResult
                name ={name}
                bmi = {bmiResult}
                result = {translateResult}/>

        </div>
    }
</div>
</div>*/
