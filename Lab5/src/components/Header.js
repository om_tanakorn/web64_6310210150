import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import {Link} from "react-router-dom"

function Header(){
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
            <Typography variant="h6"> 
                ยีดินต้อนรับสู้เว็บคำนวน BMI : 
            </Typography>
            
            <Link to="/">
                <Typography variant="body1" >
                    เครื่องคิดเลข</Typography>
                </Link>
            &nbsp;&nbsp;&nbsp;
            <Link to="/about">
                <Typography variant="body1" > 
                    ผู้จัดทำ </Typography> 
            </Link>
                    
            &nbsp;&nbsp;&nbsp;
            <Link to="/song">
                <Typography variant="body1" > 
                    เปิดเพลง </Typography>
            </Link>
            &nbsp;&nbsp;&nbsp;
            <Link to="/Lucky">
                <Typography variant="body1" > 
                    สุ่มเลข </Typography> 
            </Link>


        </Toolbar>
      </AppBar>
    </Box>
  );
}

export default Header;




/*function Header() {


    return(
        <div align ='left' id ="Header">
            ยีดินต้อนรับสู้เว็บคำนวน BMI :
            <Link to="/">เครื่องคิดเลข</Link>
            &nbsp;&nbsp;&nbsp;
            <Link to="/about">ผู้จัดทำ</Link>
            &nbsp;&nbsp;&nbsp;
            <Link to="/song">เปิดเพลง</Link>
            &nbsp;&nbsp;&nbsp;
            <Link to="/Lucky">สุ่มเลข</Link>
            <hr />


        </div>
    )
    
}*/